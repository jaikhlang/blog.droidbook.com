@extends('backend.includes.backend')
@section('stylesheets')

@endsection
@section('content')
  <div class="flex-container">
      <div class="columns m-t-10">
        <div class="column">
          <h1 class="title">Create New User</h1>
        </div>
      </div>
      <hr class="m-t-0">
      <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="columns">
          <div class="column">
            <div class="field">
              <label for="name" class="label">Name</label>
              <p class="control">
                <input type="text" class="input" name="name" id="name">
              </p>
            </div>

            <div class="field">
              <label for="email" class="label">Email:</label>
              <p class="control">
                <input type="text" class="input" name="email" id="email">
              </p>
            </div>

            <div class="field">
              <label for="password" class="label">Password</label>
              <p class="control">
                <input type="text" class="input" name="password" id="password" v-if="!auto_password" placeholder="Manually give a password to this user">
                <b-checkbox name="auto_generate" class="m-t-15" v-model="auto_password">Auto Generate Password</b-checkbox>
              </p>
            </div>


            <div class="field">
              <label for="image">Profile Image</label>
              <div class="file m-b-10">
                <label class="file-label">
                  <input class="file-input" v-on:change="onImageChange" type="file" name="image">
                  <span class="file-cta">
                    <span class="file-icon">
                      <i class="fa fa-upload"></i>
                    </span>
                    <span class="file-label">
                      Upload Image
                    </span>
                  </span>
                </label>
              </div>
              <figure class="image is-128x128" v-if="image">
                <img :src="image">
              </figure>
            </div>
          </div> <!-- end of .column -->

          <div class="column">
            <label for="roles" class="label">Roles:</label>
            <input type="hidden" name="roles" :value="rolesSelected" />

              @foreach ($roles as $role)
                <div class="field">
                  <b-checkbox v-model="rolesSelected" :native-value="{{ $role->id }}">{{ $role->display_name }}</b-checkbox>
                </div>
              @endforeach
          </div>
        </div> <!-- end of .columns for forms -->
        <div class="columns">
          <div class="column">
            <hr />
            <button class="button is-primary is-pulled-right" style="width: 250px;">Create New User</button>
          </div>
        </div>
      </form>
    </div> <!-- end of .flex-container -->
@endsection

@section('scripts')
    <script>
      var app = new Vue({
        el: '#app',
        data: {
          auto_password: true,
          rolesSelected: [{!! old('roles') ? old('roles') : '' !!}],
          image: '',
          imageName: ''
        },
        methods: {
          onImageChange(e) {
               let files = e.target.files || e.dataTransfer.files;
               if (!files.length)
                   return;
               this.createImage(files[0]);
           },
           createImage(file) {
               let reader = new FileReader();
               let vm = this;
               reader.onload = (e) => {
                   vm.image = e.target.result;
               };
               reader.readAsDataURL(file);
           }
        }
      });
    </script>
@endsection
