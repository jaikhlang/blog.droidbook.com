@extends('backend.includes.backend')
@section('stylesheets')

@endsection
@section('content')
  <div class="flex-container">
    <div class="columns m-t-10">
      <div class="column">
        <h1 class="title">This is the comments.index page</h1>
      </div>
      <div class="column">

      </div>
    </div>

    <div class="columns">
      <div class="column">
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Comments</th>
              <th>Author</th>
              <th>Post</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($comments as $comment)
              <tr>
                <td>{{ $comment->id }}</td>
                <td>{{ $comment->message }}</td>
                <td>{{ $comment->user->name }}</td>
                <td><a href="{{ route('blog.show', $comment->post->slug) }}" class="button is-small is-primary is-outlined" target="_blank">view</a></td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>

  </div> <!-- end of .flex-container -->
@endsection
@section('scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data: {

      }
    });
  </script>
@endsection
