<div class="side-menu " id="admin-side-menu">
  <aside class="menu m-t-30 m-l-10 m-r-10">
    <p class="menu-label">
      General
    </p>
    <ul class="menu-list">
      <li><a href="{{ route('dashboard') }}" class="">Dashboard</a></li>
    </ul>

    <p class="menu-label">
      Content
    </p>
    <ul class="menu-list">
      <li class="m-t-10">
        <a href="{{ route('posts.index') }}">Posts</a>
      </li>
      <li>
        <a href="{{ route('categories.index') }}">Categories</a>
      </li>
      <li>
        <a href="{{ route('tags.index') }}">Tags</a>
      </li>
      <li>
        <a href="{{ route('comments.index') }}">Comments</a>
      </li>
    </ul>

    @role('superadmin|admin')
    <p class="menu-label">
      Administration
    </p>
    <ul class="menu-list">
      <li><a href="{{route('users.index')}}" class="">Manage Users</a></li>
      <li>
        <a class="has-submenu">Roles &amp; Permissions</a>
        <ul class="submenu">
          <li><a href="{{ route('roles.index') }}" class="">Roles</a></li>
          <li><a href="{{ route('permissions.index') }}" class="">Permissions</a></li>
        </ul>
      </li>
    </ul>
    @endrole

  </aside>
</div
