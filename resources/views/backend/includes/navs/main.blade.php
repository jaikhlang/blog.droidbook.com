<nav class="navbar is-transparent has-shadow is-fixed-top">
  <div class="navbar-brand">
    <a class="navbar-item" href="{{ url('/') }}">
      <img src="{{ asset('image/logo.png') }}" alt="Droidbook Logo" width="112" height="28">
    </a>
    <div class="navbar-burger burger" data-target="navbarDroidbook">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>

  @if (Request::segment(1) == "backend")
    <a class="navbar-item is-hidden-desktop" id="admin-slideout-button">
      <span class="icon">
        <i class="fa fa-arrow-circle-right"></i>
      </span>
    </a>
  @endif

  <div id="navbarDroidbook" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="{{ url('/') }}">
        Home
      </a>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link" href="/documentation/overview/start/">
          Docs
        </a>
        <div class="navbar-dropdown is-boxed">
          <a class="navbar-item" href="/documentation/overview/start/">
            Overview
          </a>
          <a class="navbar-item" href="https://bulma.io/documentation/modifiers/syntax/">
            Modifiers
          </a>
          <a class="navbar-item" href="https://bulma.io/documentation/columns/basics/">
            Columns
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item" href="https://bulma.io/documentation/elements/box/">
            Elements
          </a>
          <a class="navbar-item is-active" href="https://bulma.io/documentation/components/breadcrumb/">
            Components
          </a>
        </div>
      </div>
    </div>

    <div class="navbar-end">
      @guest
      <a href="{{ route('login') }}" class="navbar-item">Login</a>
      @else
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">Hey {{Auth::user()->name}}</a>
        <div class="navbar-dropdown is-right" >
          <a href="#" class="navbar-item">
            <span class="icon">
              <i class="fa fa-fw fa-user-circle-o m-r-5"></i>
            </span>Profile
          </a>

          <a href="#" class="navbar-item">
            <span class="icon">
              <i class="fa fa-fw fa-bell m-r-5"></i>
            </span>Notifications
          </a>
          @role('superadmin|admin')
          <a href="{{ route('dashboard') }}" class="navbar-item">
            <span class="icon">
              <i class="fa fa-fw fa-cog m-r-5"></i>
            </span>Manage
          </a>
          @endrole
          <hr class="navbar-divider">
          <a href="{{ route('logout') }}" class="navbar-item" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            <span class="icon">
              <i class="fa fa-fw fa-sign-out m-r-5"></i>
            </span>
            Logout
          </a>
          @include('backend.includes.forms.logout')
        </div>
      </div>
      @endguest
    </div>
  </div>
</nav>
