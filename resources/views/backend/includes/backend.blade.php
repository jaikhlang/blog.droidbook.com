<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="has-navbar-fixed-top">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/backend.css') }}" rel="stylesheet">
    @yield('stylesheets')
    <!-- Stylesheets -->
</head>
<body style="background: #f4f4f4;">

    <!-- Navigation -->
    @include('backend.includes.navs.main')
    @include('backend.includes.navs.manage')
    <!-- Navigation End -->
    <div id="app">
      <div class="management-area">
        @yield('content')
      </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/backend.js') }}"></script>
    @include('backend.includes.notifications.toast')
    @yield('scripts')
</body>
</html>
