@extends('backend.includes.backend')
@section('stylesheets')

@endsection
@section('content')
  <div class="flex-container">
    <div class="columns m-t-10">
      <div class="column">
        <h1 class="title">This is the categories.index page</h1>

        <form action="{{ route('categories.store') }}" method="POST">
          {{ method_field('POST') }}
          {{ csrf_field() }}
          <section>
            <div class="field">
              <label class="label">Category Name</label>
              <div class="control">
                <input class="input" type="text" name="category_name" placeholder="Category Name">
              </div>
            </div>

              <input class="button is-primary is-outlined" type="submit" value="Create">
          </section>
        </form>
      </div>
      <div class="column">

      </div>
    </div>

    <div class="columns">
      <div class="column">
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Category</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($categories as $category)
              <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->category_name }}</td>
                <td></td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>

  </div> <!-- end of .flex-container -->
@endsection
@section('scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data: {

      }
    });
  </script>
@endsection
