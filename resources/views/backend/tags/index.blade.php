@extends('backend.includes.backend')
@section('stylesheets')

@endsection
@section('content')
  <div class="flex-container">
    <div class="columns m-t-10">
      <div class="column">
        <h1 class="title">This is the tags.index page</h1>

        <form action="{{ route('tags.store') }}" method="POST">
          {{ method_field('POST') }}
          {{ csrf_field() }}
          <section>
            <div class="field">
              <label class="label">Tag Name</label>
              <div class="control">
                <input class="input" type="text" name="tag_name" placeholder="Tag Name">
              </div>
            </div>

              <input class="button is-primary is-outlined" type="submit" value="Create">
          </section>
        </form>
      </div>
      <div class="column">

      </div>
    </div>

    <div class="columns">
      <div class="column">
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Tag</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>

            @foreach ($tags as $tag)
              <tr>
                <td>{{ $tag->id }}</td>
                <td>{{ $tag->tag_name }}</td>
                <td></td>
              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>

  </div> <!-- end of .flex-container -->
@endsection
@section('scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data: {

      }
    });
  </script>
@endsection
