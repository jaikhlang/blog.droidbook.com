@extends('backend.includes.backend')
@section('stylesheets')

@endsection
@section('content')
  <div class="flex-container">
    <div class="columns m-t-10">
      <div class="column">
        <h1 class="title">This is the posts.index page</h1>
      </div>
      <div class="column">
        <a href="{{ route('posts.create') }}" class="button is-primary is-pulled-right"><i class="fa fa-file m-r-10"></i> Create New Post</a>
      </div>
    </div>

    <posts></posts>
    {{--
    <div class="columns">
      <div class="column">
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Slug</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($posts as $post)
              <tr>
                <td>{{ $post->id }}</td>
                <td>{{ $post->title }}</td>
                <td>{{ $post->slug }}</td>
                <td></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    --}}

  </div> <!-- end of .flex-container -->
@endsection
@section('scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data: {

      }
    });
  </script>
@endsection
