@extends('backend.includes.backend')
@section('stylesheets')

@endsection
@section('content')
    <div class="flex-container">
      <div class="columns m-t-10 m-b-0">
        <div class="column">
          <h1 class="title is-admin is-4">Update Blog Post</h1>
        </div>
        <div class="column">
          {{-- <a href="{{ route('users.create') }}" class="button is-primary is-pulled-right"><i class="fa fa-user-plus m-r-10"></i> Create New User</a> --}}
        </div>
      </div>
      <form action="{{ route('posts.update', $post->id) }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="columns">
          <div class="column is-three-quarters-desktop is-three-quarters-tablet">
            <b-field>
              <b-input type="text" placeholder="Post Title" size="" v-model="title" name="title"></b-input>
            </b-field>

            <slug-widget url="{{url('/')}}" subdirectory="blog" :title="title" @copied="slugCopied" @slug-changed="updateSlug"></slug-widget>
            <input type="hidden" v-model="slug" name="slug" />

            <b-field class="m-t-20">
              <b-input type="textarea" name="excerpt" id="excerpt" value="{{ $post->excerpt }}"
                  placeholder="Excerpt of your masterpiece..." rows="3">
              </b-input>
            </b-field>

            <categories></categories>
            <tags></tags>

            <b-field class="m-t-20">
              <b-input type="textarea" name="content" id="content" value="{{ $post->content }}"
                  placeholder="Compose your masterpiece..." rows="20">
              </b-input>
            </b-field>
          </div> <!-- end of .column.is-three-quarters -->

          <div class="column is-one-quarter-desktop is-narrow-tablet">
            <div class="card">
              <div class="card-content">
                <div class="media">
                  <div class="media-left">
                    <figure class="image is-48x48">
                      <img class="is-rounded" src="https://placehold.it/50x50"/>
                    </figure>
                  </div>
                  <div class="media-content">
                    <p class="title is-5" style="margin-bottom: 0px!important">{{Auth::user()->name}}</p>
                    <p class="subitle is-6">@jaikhlang</p>
                  </div>
                </div>
              </div>

              <div class="post-status-widget widget-area">
                <div class="status">
                  <div class="status-icon">
                    <b-icon icon="file" pack="fa" size="is-medium"></b-icon>
                  </div>
                  <div class="status-details">
                    <h4><span class="status-emphasis">Draft</span> Saved</h4>
                    <p>A Few Minutes Ago</p>
                  </div>
                </div>
              </div>
              <div class="publish-buttons-widget widget-area">
                <div class="secondary-action-button">
                  <button class="button is-info is-outlined is-fullwidth">Save Draft</button>
                </div>
                <div class="primary-action-button">
                  <button class="button is-primary is-fullwidth">Publish</button>
                </div>
              </div>
            </div>
          </div> <!-- end of .column.is-one-quarter -->
        </div>
      </form>
    </div> <!-- end of .flex-container -->
@endsection
@section('scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data: {
        title: '',
        slug: '',
        excerpt: '',
        content: '',
        api_token: '{{Auth::user()->api_token}}',
        isSelected: true
      },

      mounted(){
          return this.fetchPost()
      },

      methods: {
        updateSlug: function(val) {
          this.slug = val;
        },
        slugCopied: function(type, msg, val) {
          notifications.toast(msg, {type: `is-${type}`});
        },
        fetchPost: function() {
          axios.get('/api/post', {
            params: {
              id: {{ $post->id }}
            }
          }).then((response) => {
            this.title = response.data.title
            this.slug = response.data.slug
            this.excerpt = response.data.excerpt
            this.content = response.data.content
            this.category = response.data.category
          })
        }
      }
    });
  </script>
@endsection
