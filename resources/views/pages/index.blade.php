
    @extends('layouts.main')
    @section('title', 'Blog')
    @section('metatags')

    @endsection
    @section('stylesheets')

    @endsection
    @section('content')
      <section class="section">
        <div class="container">
          <div class="columns">

              <div class="column is-two-thirds blogs">
                @foreach ($posts as $post)
                  <div class="blog">
                    <div class="blog-content">
                      <h3 class="blog-title">
                        <a href="{{ route('blog.show', $post->slug) }}">{{ $post->title }}</a>
                      </h3>
                      <div class="blog-meta">
                        <span class="blog-avatar"><img src="{{ asset('image/author/' . $post->user->photo) }}" alt=""></span>
                        <span class="blog-author"><a href="#">{{ $post->user->name }}</a></span>
                        <span class="blog-date">{{ $post->created_at->format('F j, Y') }}</span>
                      </div>
                      <p class="blog-body">{!! $post->excerpt !!}</p>
                    </div>
                    <div class="blog-footer">
                      <a href="{{ route('blog.show', $post->slug) }}">Read more</a>
                      <span class="is-pulled-right">
                        <a href="{{ route('blog.show', $post->slug) }}#respond"><span class="fa fa-comment-o"></span> Comment</a>
                        <!--a href=""><span class="fa fa-heart-o"></span> {{ $post->like_count > 1 ? $post->like_count . ' likes' : $post->like_count . ' like' }}</a-->
                      </span>
                    </div>
                  </div>
                @endforeach
              </div>

              <div class="column">
                @include('layouts.includes.widget')
              </div>
          </div>
        </div>
      </section>
    @endsection
    @section('scripts')
      <script>

      var app = new Vue({
        el: '#app',
        data: {

        }
      });

      </script>
    @endsection
