@extends('layouts.main')

@section('title', 'Blog')
@section('metatags')

@endsection
@section('stylesheets')

@endsection
@section('content')
<section class="section">
  <div class="container">
    <div class="columns">
      <div class="column is-two-thirds blogs">
        <div class="post content">
          <div class="post-cat">
            <a class="button is-small is-primary is-rounded">{{ $post->category->category_name }}</a>
          </div>
          <div class="post-title m-t-20">
            <h1 class="is-large">{{ $post->title }}</h1>
          </div>
          <!-- Post Title -->

          <div class="post-meta level is-flex-tablet m-t-20">
            <div class="level-left">
              <span class="post-avatar level-item"><img src="{{ asset('image/author/' . $post->user->photo) }}" alt=""></span>
              <span class="post-author">
                <span>
                  <span class="meta-font">by</span><br>
                  <span><a href="#">{{ $post->user->name }}</a></span>
                </span>
              </span>
            </div>

            <div class="post-date level-item">
                <span>
                  <span class="meta-font">posted on</span><br>
                  <span style="border-bottom: 1px dotted #f4f4f4;">{{ $post->created_at->diffForHumans() }}</span>
                </span>
              </div>

            <div class="level-right">
                <span class="post-comment level-item">
                  {{ $post->comment_count > 1 ? $post->comment_count .' comments' : $post->comment_count. ' comment' }}
                </span>
              </div>
          </div>
          <!-- Post Metatags -->

          <div class="post-body">
            <p>{!! $post->content !!}</p>
          </div>
          <!-- Post-Content-Body -->

          <div class="post-tags">
            <span class="meta-font">TAGGED IN</span>

            <div class="">

            </div>
            @foreach ($post->tags as $tag)
              <a href="#" class="button is-outlined is-small">{{ $tag->tag_name }}</a>
						@endforeach
          </div>
          <!-- Post Tagged in Tags -->

          <div class="post-author-details">
            <div class="box">
              <div class="is-flex">
                <figure class="image is-96x96">
                  <img class="is-rounded" src="{{ asset('image/author/' . $post->user->photo) }}">
                </figure>
                <span>
                  <a href="" class="">{{ $post->user->name }}</a>
                  <p>
                    <span class="icon is-medium">
                      <a href="#"><span class="fa fa-envelope"></span></a>
                    </span>
                    <span class="icon is-medium">
                      <a href="#"><span class="fa fa-globe"></span></a>
                    </span>
                  </p>

                </span>

              </div>
            </div>
          </div>
          <!-- Post Author Details -->

        </div>
        <!-- End of Post Content -->
      </div>
      <!-- End of Column One -->

      <div class="column">
        @include('layouts.includes.widget')
      </div>
      <!-- End of Column Two -->
    </div>
    <!-- End of Comumns -->

    <div class="columns" id="respond">
      <div class="column is-two-thirds is-narrow is-centered">
        <h4 class="is-4 comment">
          <span><i class="fa fa-comments"></i>{{ $post->comment_count > 1 ? 'There are '.$post->comment_count .'comments' : 'There is '.$post->comment_count.' comment' }}</span>
        </h4>
        @include('layouts.includes.comment')
      </div>
      <!-- End of Comumn -->
    </div>
    <!-- End of Comumns -->
  </div>
</section>
@endsection
@section('scripts')

@endsection
