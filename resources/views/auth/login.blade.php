@extends('layouts.main')

@section('content')
<div class="container">
    <section class="columns">
        <div class="column is-one-third is-offset-one-third">
            <div class="m-t-50">
                <div class="box">
                    <form class="" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="field">
                          <label class="label">E-Mail Address</label>
                          <div class="control">
                            <input class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" id="email" type="email" placeholder="email@example.com" value="{{ old('email') }}" required autofocus>
                          </div>
                          @if ($errors->has('email'))
                          <p class="help is-success">{{ $errors->first('email') }}</p>
                          @endif
                        </div>

                        <div class="field">
                          <label for="password" class="label">Password</label>
                            <input class="input {{ $errors->has('password') ? 'is-danger' : '' }}" type="password" name="password" id="password" required>
                          @if ($errors->has('password'))
                            <p class="help is-danger">{{ $errors->first('password') }}</p>
                          @endif
                        </div>

                        <div class="field">
                          <button type="submit" class="button is-primary is-outlined is-fullwidth">
                              Login
                          </button>
                        </div>

                        <div class="field">
                          <a class="" href="{{ route('password.request') }}">
                              Forgot Your Password?
                          </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
  <script>
    var app = new Vue({
      el: '#app',
      data: {
        checkbox: false
      }
    });
  </script>
@endsection
