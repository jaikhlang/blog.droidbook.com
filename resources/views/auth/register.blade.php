@extends('layouts.main')
@section('stylesheets')

@endsection
@section('content')
<div class="container">
    <div class="columns">
        <div class="column is-one-third is-offset-one-third">
            <div class="m-t-50">
                <div class="box">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="field">
                          <label class="label">Name</label>
                          <div class="control">
                            <input class="input {{ $errors->has('name') ? 'is-danger' : '' }}" name="name" id="name" type="text" placeholder="Name" value="{{ old('name') }}" required>
                          </div>
                          @if ($errors->has('name'))
                          <p class="help is-success">{{ $errors->first('name') }}</p>
                          @endif
                        </div>

                        <div class="field">
                          <label class="label">E-Mail Address</label>
                          <div class="control">
                            <input class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" id="email" type="email" placeholder="email@example.com" value="{{ old('email') }}" required>
                          </div>
                          @if ($errors->has('email'))
                          <p class="help is-success">{{ $errors->first('email') }}</p>
                          @endif
                        </div>

                        <div class="field">
                          <label class="label">Password</label>
                          <div class="control">
                            <input class="input {{ $errors->has('password') ? ' is-danger' : '' }}" name="password" id="password" type="password" value="{{ old('password') }}" required>
                          </div>
                          @if ($errors->has('password'))
                          <p class="help is-success">{{ $errors->first('password') }}</p>
                          @endif
                        </div>

                        <div class="field">
                          <label class="label">Confirm Password</label>
                          <div class="control">
                            <input class="input {{ $errors->has('password') ? 'is-danger' : '' }}" name="password_confirmation" id="password-confirm" type="password" required>
                          </div>
                        </div>

                        <div class="field">
                            <button type="submit" class="button is-fullwidth is-primary is-outlined">
                                Register
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
