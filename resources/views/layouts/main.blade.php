<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>DroidBook - @yield('title', 'Blog')</title>

    @yield('metatags')
    <!-- Metatags -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('stylesheets')
    <!-- Stylesheets -->
  </head>
  <body>
    @include('layouts.includes.nav')
    <!-- Navigation -->
    <div id="app">
      @yield('content')
    </div>
    <!-- Contents -->
    @include('layouts.includes.footer')
    <!-- Footer -->
  </body>
  <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
  @include('backend.includes.notifications.toast')
    @yield('scripts')
    <!-- Java Scripts -->
</html>
