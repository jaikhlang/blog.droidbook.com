
@foreach ($post->comments as $comment)
<article class="media m-t-15">
  <figure class="media-left">
    <p class="image is-64x64">

      <img class="is-rounded" src="{{ asset('https://www.gravatar.com/avatar/' . md5(strtolower(trim($comment->user->email))) . '?d=' . urlencode('wavatar') . '&s=' . 128) }}" alt="">
    </p>
  </figure>
  <div class="media-content">


      <div class="content">
        <p>
          <strong>{{ $comment->user->name }}</strong>
          <br>
          {{ $comment->message }}
          <br>
          <small>{{--<a>Like</a> · <a>Reply</a> · --}} {{ $comment->created_at->diffForHumans() }}</small>
        </p>
      </div>

    {{--
    <article class="media">
      <figure class="media-left">
        <p class="image is-48x48">
          <img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png">
        </p>
      </figure>
      <div class="media-content">
        <div class="content">
          <p>
            <strong>Sean Brown</strong>
            <br>
            Donec sollicitudin urna eget eros malesuada sagittis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam blandit nisl a nulla sagittis, a lobortis leo feugiat.
            <br>
            <small><a>Like</a> · <a>Reply</a> · 2 hrs</small>
          </p>
        </div>

        <article class="media">
          Vivamus quis semper metus, non tincidunt dolor. Vivamus in mi eu lorem cursus ullamcorper sit amet nec massa.
        </article>

        <article class="media">
          Morbi vitae diam et purus tincidunt porttitor vel vitae augue. Praesent malesuada metus sed pharetra euismod. Cras tellus odio, tincidunt iaculis diam non, porta aliquet tortor.
        </article>
      </div>
    </article>

    <article class="media">
      <figure class="media-left">
        <p class="image is-48x48">
          <img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png">
        </p>
      </figure>
      <div class="media-content">
        <div class="content">
          <p>
            <strong>Kayli Eunice </strong>
            <br>
            Sed convallis scelerisque mauris, non pulvinar nunc mattis vel. Maecenas varius felis sit amet magna vestibulum euismod malesuada cursus libero. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Phasellus lacinia non nisl id feugiat.
            <br>
            <small><a>Like</a> · <a>Reply</a> · 2 hrs</small>
          </p>
        </div>
      </div>
    </article>
    --}}
  </div>
</article>
@endforeach
<div class="comment-box box m-t-20">

  <form class="" action="{{ route('comments.store', $post->id) }}" method="POST">
    {{ method_field('POST') }}
    {{ csrf_field() }}

      @guest
      <a href="{{ route('login') }}" class="button is-small is-primary">Sign in to comment</a>
      <!--div class="columns m-t-10">
        <div class="column">
          <div class="field">
            <input type="text" class="input is-primary" name="name" value="" placeholder="Name">
          </div>
        </div>
        <div class="column">
          <div class="field">
            <input type="email" class="input is-primary" name="email" value="" placeholder="Email">
          </div>
        </div>
      </div-->
      @else
      <textarea class="textarea is-primary" name="message" rows="2" placeholder="Comment here"></textarea>

      <div class="field m-t-10">
        <input type="submit" class="button is-primary" name="" value="Post Comment">
      </div>
      @endguest
  </form>
</div>
