<div class="widget">
  <h4 class="widget-title">
    <span>POPULAR CATEGORIES</span>
  </h4>
  <div class="category-box">
    <ul>
      @foreach ($categories as $category)
          <li>
            <div>
              <a href="{{ route('blog.categories', ['category' => $category->id]) }}" class="button is-primary is-rounded">{{ $category->category_name }}</a>
              <br><small>{{ $category->posts->count() > 1 ? $category->posts->count() . ' posts' : $category->posts->count() . ' post'}}</small>
            </div>
          </li>
      @endforeach
    </ul>
  </div>
</div>

<div class="widget">
  <h4 class="widget-title">
    <span>POPULAR POSTS</span>
  </h4>
  <div class="panel popular-posts">
    <?php $i = 1 ?>
    @foreach ($posts as $post)
      <a class="panel-block" href="{{ route('blog.show', $post->slug) }}">
        <div class="m-r-10 post-number">
          <span>{{ $i++ }}</span>
        </div>
        <div class="">
          <span>{{ $post->title }}</span><br>
          <small style="font-size: 11px; font-weight: normal;">{{ $post->created_at->diffForHumans() }}</small>
        </div>
      </a>
    @endforeach
  </div>
</div>
