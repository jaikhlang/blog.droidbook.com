<nav class="navbar is-fixed-top is-transparent">
  <div class="container">
    <div class="navbar-brand">
      <a class="navbar-item" href="{{ url('/') }}">
        <img src="{{ asset('image/logos.png') }}" alt="DroidBook Logo" height="28">
      </a>
      <div class="navbar-burger burger" data-target="navbarDroidbook">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <div id="navbarDroidbook" class="navbar-menu">
      <div class="navbar-start">
        <a class="navbar-item" href="{{ asset('/') }}">Blog</a>
        <!--a class="navbar-item" href="{{ asset('/') }}">Learn</a>
        <a class="navbar-item" href="{{ asset('/') }}">Discuss</a>
        <a class="navbar-item" href="{{ asset('/') }}">Share</a-->
      </div>

      <div class="navbar-end">
        @guest
        <a class="navbar-item" href="{{ asset('login') }}">Login</a>
        <a class="navbar-item" href="{{ asset('register') }}">Join The Community</a>
        @else
          <div class="navbar-item has-dropdown is-hoverable">
            <a class="navbar-link">{{ Auth::user()->name }}</a>
            <div class="navbar-dropdown is-right" >

              @role('superadmin|admin')
              <a href="#" class="navbar-item">
                <span class="icon">
                  <i class="fa fa-fw fa-user-circle-o m-r-5"></i>
                </span>Profile
              </a>
              <a href="{{ route('dashboard') }}" class="navbar-item">
                <span class="icon">
                  <i class="fa fa-fw fa-cog m-r-5"></i>
                </span>Manage
              </a>
              <a href="#" class="navbar-item">
                <span class="icon">
                  <i class="fa fa-fw fa-bell m-r-5"></i>
                </span>Notifications
              </a>
              <hr class="navbar-divider">
              @endrole


              <a href="{{ route('logout') }}" class="navbar-item" onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                <span class="icon">
                  <i class="fa fa-fw fa-sign-out m-r-5"></i>
                </span>
                Logout
              </a>
              @include('backend.includes.forms.logout')
            </div>
          </div>
        @endguest
      </div>
    </div>
  </div>
</nav>
