<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function () {
  Route::get('/posts/unique', 'PostController@apiCheckUnique')->name('api.posts.unique');
});

Route::get('/posts', 'ApiController@apiPosts')->name('api.posts.index');
Route::get('/post', 'ApiController@apiPost')->name('api.posts.edit');
Route::get('/categories', 'ApiController@apiCategories')->name('api.categories.index');
Route::get('/tags', 'ApiController@apiTags')->name('api.tags.index');

Route::post('/posts', 'ApiController@apiStorePost')->name('api.posts.store');
