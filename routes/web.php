<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Deploy
Route::get('/deploy', 'DeployController@deploy')->name('deploy');

//Frontend Routes
Route::get('/', 'PageController@index')->name('index');
Route::get('/blog', 'PageController@getPostsByCategory')->name('blog.categories');
Route::get('blog/{slug}', 'PageController@show')->name('blog.show');
Route::post('/comments/{id}', 'CommentController@store')->name('comments.store');

//Backend Routes
Route::get('admin', function(){
  return redirect()->route('dashboard');
});
Route::group(['prefix' => 'backend'], function(){
  Route::get('/dashboard', 'BackendController@index')->name('dashboard');
  Route::resource('/users', 'UserController');
  Route::resource('/roles', 'RoleController');
  Route::resource('/permissions', 'PermissionController');
  Route::resource('/posts', 'PostController');
  Route::resource('/categories', 'CategoryController');
  Route::resource('/tags', 'TagController');
  Route::resource('/comments', 'CommentController', [ 'except' => ['store'] ]);
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
