<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Tag;

class ApiController extends Controller
{
  public function apiPosts(){
    $posts = Post::get();
    return $posts;
  }

  public function apiPost(Request $request){
    $post = Post::where('id', $request->id)->first();
    return $post;
  }

  public function apiCategories(){
    $categories = Category::get();
    return $categories;
  }

  public function apiTags(){
    $tags = Tag::get();
    return $tags;
  }

  public function apiStorePost(Request $request){
    /*dd($request);
    $post = new Post;
    $post->title = $request->title;
    $post->slug = $request->slug;
    $post->excerpt = $request->excerpt;
    $post->content = $request->content;
    $post->title = $request->title;
    $post->author_id = Auth::user()->id;
    $post->save();
    */

    return $post;
  }
}
